require 'date'

def rand_time
  Time.at(rand * Time.now.to_i)
end

people = [[2, 'matayo'], [1, 'nico'], [0, 'angelo'], [3, 'luca']]

logins = { # ugly on purpose
           0 => [rand_time, rand_time, rand_time, rand_time, rand_time, rand_time, rand_time, rand_time],
           1 => [rand_time, rand_time, rand_time, rand_time, rand_time, rand_time, rand_time, rand_time],
           2 => [rand_time, rand_time, rand_time, rand_time, rand_time, rand_time, rand_time, rand_time],
           3 => [rand_time, rand_time, rand_time, rand_time, rand_time, rand_time, rand_time, rand_time],
}




people_flatten = people.flatten

logins_per_year = {}

logins.each do |user, entry|
  # hash for the users
  login_user = {}
  # iterate through the entries
  entry.each do |date|
    year = date.year
    login_user.include?(year) ? login_user[year] += 1 : login_user[year] = 1
  end
  # find user name, as the array is flattered all the names are id index + 1
  name_position = people_flatten.index(user) + 1
  # add login_user to logins_per_year hash
  logins_per_year[people_flatten[name_position]] = login_user

end

puts logins_per_year



